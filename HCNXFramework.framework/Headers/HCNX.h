//
//  HCNX.h
//  HCNXFramework
//
//  Created by Guillaume MARTINEZ on 21/03/2017.
//  Copyright © 2017 Guillaume MARTINEZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HCNXBase/HCNXBase.h>
#import <HelloFramework/Hello.h>
#import <MGSFramework/MGSFramework.h>
#import <OTPFramework/OTP.h>
#import <VersionFramework/Version.h>

@interface HCNX : HCNXBase

+ (HCNX *)sharedInstance;
- (void)configureWithAPIKey:(NSString*)pApiKey andHmacKey:(NSString*)pHmacKey;
- (NSString*)getHnId;
- (void)enableTestEnvironment;
- (void)showLog:(BOOL)pEnable;
- (void)clearConfiguration;

@property (nonatomic, retain) HCNXBase * base;
@property (nonatomic, retain) Hello * hello;
@property (nonatomic, retain) MGSFramework * mgs;
@property (nonatomic, retain) Version * version;
@property (nonatomic, retain) OTP * otp;


@end
