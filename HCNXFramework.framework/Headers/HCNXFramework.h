//
//  HCNXFramework.h
//  HCNXFramework
//
//  Created by Guillaume MARTINEZ on 21/03/2017.
//  Copyright © 2017 Guillaume MARTINEZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCNX.h"

//! Project version number for HCNXFramework.
FOUNDATION_EXPORT double HCNXFrameworkVersionNumber;

//! Project version string for HCNXFramework.
FOUNDATION_EXPORT const unsigned char HCNXFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HCNXFramework/PublicHeader.h>


